# Linea102

Sistema de recoleccion de informacion del servicio telefónico gratuito de atención pública y permanente  para recepcionar consultas, información y/o notificaciones sobre situaciones de vulneración de derechos  de niños, niñas y adolescentes.

Para poder probar este proyecto solo deben Instalar:
*  Python3+

Correr:
*  pip install -r requeriments.txt
*  python3 manage.py createsuruser
*  python3 manage.py runserver

Podran accederlo via: http://localhost:8000
y al admin via: http://localhost:8000/admin