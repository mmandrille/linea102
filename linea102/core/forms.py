from django import forms

#Definimos nuestros formularios
class UploadCsv(forms.Form):
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))