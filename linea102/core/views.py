from django.shortcuts import render

#Import Personales
from core.models import Departamento, Localidad
from core.forms import UploadCsv
# Create your views here.
def home(request):
    return render(request, 'home.html', {})


def upload_localidades(request):
    if request.method == "GET":
        titulo = "Carga Masiva Geografica"
        form = UploadCsv()
        return render(request, "extras/upload_csv.html", {'titulo': titulo, 'form': form, })
    else:
        form = UploadCsv(request.POST, request.FILES)
        if form.is_valid():
            file_data = form.cleaned_data['csvfile'].read().decode("utf-8")
            lines = file_data.split("\n")
            cant = 0
            for linea in lines:
                cant += 1
                linea=linea.split(',')
                if linea[0]:
                    departamento = Departamento.objects.get_or_create(
                        alpha_id= linea[0],
                        nombre= linea[1])[0]
                    localidad = Localidad.objects.get_or_create(
                        departamento= departamento,
                        alpha_id= linea[2],
                        nombre= linea[3])[0]
            return render(request, 'extras/upload_csv.html', {'count': len(lines), })
        else:
            message = form.errors
            return render(request, "extras/upload_csv.html", {'message': message, })
