from django.contrib import admin
#Modulos para no permitir escalacion de privilegios
from django.contrib.auth.admin import UserAdmin, User

#Incluimos modelos
from .models import Departamento, Localidad
from .models import Problematica, Situacion, Consultante
from .models import Denunciante, Asistido, Profesional
from .models import OficinaOpd, Responsable
from .models import Llamada

#Modificacion del panel de administrador para no permitir escalacion de privilegios
class RestrictedUserAdmin(UserAdmin):
    model = User
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(RestrictedUserAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        user = kwargs['request'].user
        if not user.is_superuser:
            if db_field.name == 'groups':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.groups.all()])
            if db_field.name == 'user_permissions':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.user_permissions.all()])
            if db_field.name == 'is_superuser':
                field.widget.attrs['disabled'] = True
        return field

#Models Ocultos
def register_hidden_models(*model_names):
    for m in model_names:
        ma = type(
            str(m)+'Admin',
            (admin.ModelAdmin,),
            {
                'get_model_perms': lambda self, request: {}
            })
        admin.site.register(m, ma)

#Definimos inlines
class DenuncianteInline(admin.TabularInline):
    model = Denunciante
    insert_after = 'tipo_llamada'
    min_num = 1
    extra = 0

class AsistidoInline(admin.TabularInline):
    model = Asistido
    insert_after = 'problematica'
    min_num = 1
    extra = 0

# Modificamos admin view
class DepartamentodAdmin(admin.ModelAdmin):
    ordering = ('alpha_id',)
    search_fields = ['nombre']

class LocalidadAdmin(admin.ModelAdmin):
    ordering = ('alpha_id',)
    search_fields = ['nombre']
    list_filter = ['departamento']

class ProblematicaAdmin(admin.ModelAdmin):
    ordering = ('nombre',)
    search_fields = ['nombre']

class SituacionAdmin(admin.ModelAdmin):
    ordering = ('nombre',)
    search_fields = ['nombre']

class PersonaAdmin(admin.ModelAdmin):
    ordering = ('apellidos', 'nombres')
    search_fields = ['apellidos', 'nombres']

class LlamadaAdmin(admin.ModelAdmin):
    inlines = [DenuncianteInline, AsistidoInline]
    search_fields = ['id', 'asistido__nombres', 'asistido__apellidos', 'localidad', 'descripcion']
    autocomplete_fields = ['localidad', 'problematica']
    list_filter = ['localidad', 'localidad__departamento', 'tipo_contacto', 'tipo_llamada', 'derivado']
    #Para poder cambiar el orden de los campos:
    change_form_template = 'admin/custom/change_form.html'
    class Media:
        css = {
            'all': (
                'css/admin.css',
            )
        }

#Registramos modificaciones para no permitir escalacion de privilegios
admin.site.unregister(User)
admin.site.register(User, RestrictedUserAdmin)
# Register your models here.
admin.site.register(Departamento, DepartamentodAdmin)
admin.site.register(Localidad, LocalidadAdmin)
admin.site.register(Problematica, ProblematicaAdmin)
admin.site.register(Situacion, SituacionAdmin)
admin.site.register(Consultante)
admin.site.register(Denunciante, PersonaAdmin)
admin.site.register(Asistido, PersonaAdmin)
admin.site.register(Profesional)
admin.site.register(OficinaOpd)
admin.site.register(Responsable)
admin.site.register(Llamada, LlamadaAdmin)
