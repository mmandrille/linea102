#Import Standards
from django.db import models
#Import Plugins
from tinymce.models import HTMLField

#Choice Fields
TIPO_CONTACTO = (
        (1, 'Individuo'),
        (2, 'Institución'),
        (3, 'Agente Externo'),
        (4, 'Interna'),
        (5, 'Residual'),       
)

TIPO_LLAMADA = (
        (1, 'Emergencia'),
        (2, 'Informativo'),
        (3, 'Llamada Residual'),
        (4, 'Urgencia'),
)

# Create your models here.
class Departamento(models.Model):
    alpha_id = models.IntegerField(default=0)
    nombre = models.CharField('Nombre', max_length=25, blank=True, null=True)
    def __str__(self):
        return self.nombre

class Localidad(models.Model):
    departamento = models.ForeignKey(Departamento, on_delete=models.SET_NULL, blank=True, null=True)
    alpha_id = models.IntegerField(default=0)
    nombre = models.CharField('Nombre', max_length=25, blank=True, null=True)
    def __str__(self):
        return self.nombre

class Problematica(models.Model):
    nombre = models.CharField('Nombre', max_length=200, blank=True, null=True)
    descripcion = HTMLField()
    def __str__(self):
        return self.nombre

class Situacion(models.Model):
    nombre = models.CharField('Nombre', max_length=200, blank=True, null=True)
    descripcion = HTMLField()
    def __str__(self):
        return self.nombre

class Consultante(models.Model):
    nombre = models.CharField('Nombre', max_length=8, blank=True, null=True)
    def __str__(self):
        return self.nombre

class Profesional(models.Model):
    apellidos = models.CharField('Apellidos', max_length=50, blank=True, null=True)
    nombres = models.CharField('Nombres', max_length=50, blank=True, null=True)
    titulo = models.CharField('Titulo Profesional', max_length=50, blank=True, null=True)
    domicilio = models.CharField('Domicilio', max_length=25, blank=True, null=True)
    telefono = models.CharField('Numero de Contacto', max_length=25, blank=True, null=True)
    def __str__(self):
        return self.nombres + ' ' + self.apellidos + ' (' + self.titulo + ')'

class Responsable(models.Model):
    apellidos = models.CharField('Apellidos', max_length=50, blank=True, null=True)
    nombres = models.CharField('Nombres', max_length=50, blank=True, null=True)
    titulo = models.CharField('Titulo Profesional', max_length=50, blank=True, null=True)
    domicilio = models.CharField('Domicilio', max_length=25, blank=True, null=True)
    telefono = models.CharField('Numero de Contacto', max_length=25, blank=True, null=True)
    def __str__(self):
        return self.nombres + ' ' + self.apellidos + ' (' + self.titulo + ')'

class OficinaOpd(models.Model):
    nombres = models.CharField('Nombres', max_length=50, blank=True, null=True)
    domicilio = models.CharField('Direccion', max_length=25, blank=True, null=True)
    telefono = models.CharField('Telefono de Contacto', max_length=25, blank=True, null=True)
    responsable = models.ForeignKey(Responsable, on_delete=models.SET_NULL, blank=True, null=True)
    def __str__(self):
        return self.nombres

class Llamada(models.Model):
    numero = models.AutoField(primary_key=True)
    fecha = models.DateTimeField(auto_now_add=True)
    localidad = models.ForeignKey(Localidad, on_delete=models.CASCADE, related_name="llamadas", null=True, blank=True)
    barrio = models.CharField('Barrio', max_length=50, blank=True, null=True)
    tipo_contacto = models.IntegerField(choices=TIPO_CONTACTO, default=1)
    tipo_llamada = models.IntegerField(choices=TIPO_LLAMADA, default=1)
    otro_denunciante = models.CharField('Otro Denunciante', max_length=100, blank=True, null=True)
    relacionada = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True)
    motivo_relacion = models.CharField('Motivo de Relacion', max_length=100, blank=True, null=True)
    situacion = HTMLField()
    problematica = models.ForeignKey(Problematica, on_delete=models.SET_NULL, related_name="llamadas", blank=True, null=True)
    consultante = models.ForeignKey(Consultante, on_delete=models.SET_NULL, blank=True, null=True)
    respuesta = HTMLField()
    profesional1 = models.ForeignKey(Profesional, verbose_name='Profesional Derivado', on_delete=models.SET_NULL, related_name="llamadas", blank=True, null=True)
    profesional2 = models.ForeignKey(Profesional, verbose_name='Profesional Secundario',on_delete=models.SET_NULL, related_name="llamadas2", blank=True, null=True)
    derivado = models.ForeignKey(OficinaOpd, verbose_name='OPD Derivado', on_delete=models.SET_NULL, related_name="llamadas", blank=True, null=True)
    audio = models.FileField('Audio de llamada', upload_to='audios/', null=True, blank=True)
    ##ALMACENAR USUARIOS
    def __str__(self):
        return str(self.numero) + ': ' + str(self.fecha)[:16] + ' - ' + self.situacion[:40]

class Denunciante(models.Model):
    llamada = models.OneToOneField(Llamada, on_delete=models.CASCADE, related_name="denunciantes", null=True, blank=True)
    apellidos = models.CharField('Apellidos', max_length=50, blank=True, null=True)
    nombres = models.CharField('Nombres', max_length=50, blank=True, null=True)
    domicilio = models.CharField('Domicilio del que llama', max_length=25, blank=True, null=True)
    telefono = models.CharField('Numero de Contacto', max_length=25, blank=True, null=True)
    relacion = HTMLField()
    def __str__(self):
        return self.nombres + ' ' + self.apellidos

class Asistido(models.Model):
    llamada = models.OneToOneField(Llamada, on_delete=models.CASCADE, related_name="asistidos", null=True, blank=True)
    apellidos = models.CharField('Apellidos', max_length=50, blank=True, null=True)
    nombres = models.CharField('Nombres', max_length=50, blank=True, null=True)
    domicilio = models.CharField('Domicilio', max_length=25, blank=True, null=True)
    edad =  models.PositiveIntegerField()
    discapacidad = models.CharField('Discapacidad', max_length=100, blank=True, null=True)
    def __str__(self):
        return self.nombres + ' ' + self.apellidos