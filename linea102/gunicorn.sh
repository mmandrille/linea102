#!/bin/bash
cd /opt/linea102
source venv/bin/activate
cd /opt/linea102/linea102
gunicorn linea102.wsgi -t 600 -b 127.0.0.1:8009 -w 6 --user=servidor --group=servidor --log-file=/opt/linea102/gunicorn.log 2>>/opt/linea102/gunicorn.log

